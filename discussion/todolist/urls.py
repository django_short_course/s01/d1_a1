from django.urls import path
# import the views.py from the same folder
from . import views

urlpatterns = [
    path('index', views.index, name = 'index'),
    path('<int:todoitem_id>/', views.todoitem, name = "viewtodoitem")
]