from django.shortcuts import render

# Create your views here.

# THe from keyword allows importing of necessary classes/ modules, methods and others files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse
# Local imports
from .models import ToDoItem
# to use the template created:
from django.template import loader

def index(request):
    todoitem_list = ToDoItem.objects.all()
    template = loader.get_template("index.html")
    context = {
        'todoitem_list': todoitem_list
    }
    # output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
    return HttpResponse(template.render(context, request))

def todoitem(request, todoitem_id):
    response = f"You are viewing the details of {todoitem_id}"
    return HttpResponse(response)