from django.urls import path
from . import views

urlpatterns = [
    path('<int:groceryitem_id>/', views.groceryitem, name = 'grocery'),
    path('index', views.index, name = 'index')
]