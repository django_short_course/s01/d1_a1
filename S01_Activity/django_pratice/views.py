from django.shortcuts import render
from django.http import HttpResponse
from .models import GroceryItem
from django.template import loader
# Create your views here.
def index(request):
    groceryitem_list = GroceryItem.objects.all()
    template = loader.get_template("index.html")
    context = {
        'groceryitem_list' : groceryitem_list
    }
    return HttpResponse(template.render(context, request))

def groceryitem(request, groceryitem_id):
    res = f"You are viewing the details of {groceryitem_id}"
    return HttpResponse(res)